## Central Park Chronicles: Your Premier Friends-Themed Marketplace and Events Hub
Central Park Chronicles is a comprehensive online platform designed to cater to fans of the popular TV series “Friends”.This project aims to create an immersive experience where users can register, purchase Friends-themed products. The platform will also act as an events hub, providing users with information about upcoming events. The platform's primary goal is to connect fans.

